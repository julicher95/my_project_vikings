﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using Invector.vCharacterController;


public class footstep : MonoBehaviour
{
    [FMODUnity.EventRef] public string footEvent;
   // [FMODUnity.EventRef] public string runEvent;
    FMOD.Studio.EventInstance eventInstance;
    vThirdPersonInput July;
    vThirdPersonController Julia;
    RaycastHit rh; // переменная для поверхности 
    public LayerMask lm; // слой
    public float Material = 1f;
    public float LocomotionType = 0f;

    // Start is called before the first frame update
    void Start()
    {
        July = GetComponent<vThirdPersonInput>();    
        Julia = GetComponent<vThirdPersonController>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void footsteps()
    {
        if (July.cc.input.magnitude > 0.1)
        {
            MaterialCheck();
            
            eventInstance = RuntimeManager.CreateInstance(footEvent);
            RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
            if (Julia.isSprinting) LocomotionType = 1f;

            else LocomotionType = 0f;

            eventInstance.setParameterByName("LocomotionType", LocomotionType);
            eventInstance.setParameterByName("Material", Material);
            eventInstance.start();
            eventInstance.release();

        }

    }
    void MaterialCheck() // проверка поверхности
    {
        if (Physics.Raycast(transform.position, Vector3.down, out rh, 0.1f, lm))
        {
            Debug.Log(rh.collider.tag); // в консоли будет показывать куда мы наступили
            if (rh.collider.tag == "Wood") Material = 1f;
            else if (rh.collider.tag == "Snow") Material = 0f;
            else if (rh.collider.tag == "Water") Material = 2f;
            else if (rh.collider.tag == "Prop") Material = 3f;
            else Material = 0f;

        }
    }
}
